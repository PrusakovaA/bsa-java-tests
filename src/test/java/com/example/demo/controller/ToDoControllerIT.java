package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.dto.mapper.ToDoEntityToRequestMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoService toDoService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void whenSave_thenReturnValidResponse() throws Exception {
        String testText = "My to do text";
        Long testId = 1l;
        when(toDoService.upsert(ArgumentMatchers.any(ToDoSaveRequest.class)))
                .thenReturn(ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText)));
        this.mockMvc
                .perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(ToDoEntityToRequestMapper.map(new ToDoEntity(testText)))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$.text").value(testText))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(testId))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenGetTextWithNullId_thenBadRequest() throws Exception {
        Long id = null;

        when(toDoService.getTextById(id)).thenThrow(new NumberFormatException());

        this.mockMvc
                .perform(get("/todos/text/{id}", id))
                .andExpect(status().isBadRequest());
    }
}