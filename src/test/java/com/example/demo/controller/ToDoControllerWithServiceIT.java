package com.example.demo.controller;

import com.example.demo.dto.mapper.ToDoEntityToRequestMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
public class ToDoControllerWithServiceIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void whenSave_thenReturnValidResponse() throws Exception {
        String testText = "To do text";
        Long testId = 1l;
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class)))
                .thenReturn(new ToDoEntity(testId, testText));
        this.mockMvc
                .perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(ToDoEntityToRequestMapper.map(new ToDoEntity(testText)))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$.text").value(testText))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(testId))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenGetOne_thenReturnCorrectOne() throws Exception {
        String testText = "Test text";
        Long testId = 1l;
        var todo = new ToDoEntity(testId, testText);
        when(toDoRepository.findById(ArgumentMatchers.anyLong())).thenReturn(Optional.of(todo));

        this.mockMvc
                .perform(get("/todos/" + todo.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(todo.getId()))
                .andExpect(jsonPath("$.text").value(todo.getText()));
    }

    @Test
    void whenGetText_thenReturnValidResponse() throws Exception {
        Long testId = 1l;
        String testText = "Test text";

        when(toDoRepository.findById(testId)).thenReturn(Optional.of(new ToDoEntity(testId, testText)));

        this.mockMvc
                .perform(get("/todos/text/{id}", testId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$").isString())
                .andExpect(jsonPath("$").value(testText))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }


}
