package com.example.demo.service;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.util.Optional;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

class ToDoServiceTest {

    private ToDoRepository toDoRepository;

    private ToDoService toDoService;

    @BeforeEach
    void setUp() {
        this.toDoRepository = mock(ToDoRepository.class);
        toDoService = new ToDoService(toDoRepository);
    }

    @Test
    void whenUpsertWrongId_thenToDoNotFoundException() {
        var id = 400L;
        var toDoDto = new ToDoSaveRequest();
        toDoDto.id = id;
        assertThrows(ToDoNotFoundException.class, () -> toDoService.upsert(toDoDto));
    }

    @Test
    void whenGetTextById_thenReturnCorrectOne() throws ToDoNotFoundException {
        //mock
        var todo = new ToDoEntity(0l, "Test 1");
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(todo));

        //call
        var result = toDoService.getTextById(0l);

        //validate
        assertEquals(result, todo.getText());
    }

    @Test
    void whenSaveToDo_thenItHasTheText() throws ToDoNotFoundException {

        //mock
        var expectedToDo = new ToDoEntity(0l, "Text");
        when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
            Long id = i.getArgument(0, Long.class);
            if (id.equals(expectedToDo.getId())) {
                return Optional.of(expectedToDo);
            } else {
                return Optional.empty();
            }
        });
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
            ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
            Long id = arg.getId();
            if (id != null) {
                if (!id.equals(expectedToDo.getId()))
                    return new ToDoEntity(id, arg.getText());
                expectedToDo.setText(arg.getText());
                return expectedToDo;
            } else {
                return new ToDoEntity(40158l, arg.getText());
            }
        });

        //call
        var toDoSaveRequest = new ToDoSaveRequest();
        toDoSaveRequest.id = expectedToDo.getId();
        toDoSaveRequest.text = "Text";
        var todo = toDoService.upsert(toDoSaveRequest);

        //validate
        assertNotNull(todo.text);
        assertEquals(expectedToDo.getText(), todo.text);
    }


}