package com.example.demo.dto.mapper;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.model.ToDoEntity;

public class ToDoEntityToRequestMapper {
    public static ToDoSaveRequest map(ToDoEntity todoEntity) {
        if (todoEntity == null)
            return null;
        var result = new ToDoSaveRequest();
        result.id = todoEntity.getId();
        result.text = todoEntity.getText();
        return result;
    }
}
